var svgDOM = null

$(function () {
  Viewport.on_resize()
  $(window).resize(Viewport.on_resize)
  /*
  if (mobile) {
    setInterval(on_resize, 1000)
  }
  */
})

window.onload = function () {
  Coloso.init()
  UI.init()
  Frames.init()
}
