var height = null
var width = null
var mobile = false
var Viewport = {
  on_resize: function () {
    // Si es celu lo ajusto al ancho si es desktop lo ajusto al alto
    if ($(window).width() > 980) {
      height = $(window).height()
      width = (1080 * (height / 1920))
    } else {
      width = $(window).width()
      height = (1920 * (width / 1080))
      mobile = true
    }

    $('#content').css({
      width: width,
      height: height
    })
    $('body').css('font-size', height * 16 / 976)
  }
}
